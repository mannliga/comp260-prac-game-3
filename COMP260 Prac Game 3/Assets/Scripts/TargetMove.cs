﻿using UnityEngine;
using System.Collections;

public class TargetMove : MonoBehaviour {

	private Animator animator;
	public float startTime = 0.0f;

	// Use this for initialization
	void Start () {
	//get the animator component
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	//set the Start parameter to true
		//iff we have passed the start time
		animator.SetBool("Start", Time.time>= startTime);
	}

	void Destroy () {
		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision) {
		Debug.Log ("SLDKJFSLKDJF");
		animator.SetBool ("Hit", true);
	}
}
