﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Fire1")) {

			BulletMove bullet = Instantiate (bulletPrefab);
			bullet.transform.position = transform.position;

			Ray ray = 
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
	}
}
}
